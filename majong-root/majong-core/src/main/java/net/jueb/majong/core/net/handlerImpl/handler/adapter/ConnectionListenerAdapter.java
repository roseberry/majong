package net.jueb.majong.core.net.handlerImpl.handler.adapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.NetConnectionListener;
import net.jueb.majong.core.net.handlerImpl.NetConntectionAdapter;
import net.jueb.util4j.net.JConnection;
import net.jueb.util4j.net.JConnectionListener;

/**
 * 心跳链接监听
 * @author Administrator
 */
public abstract class ConnectionListenerAdapter<T> implements JConnectionListener<T>,NetConnectionListener<T>{

	protected final Logger _log = LoggerFactory.getLogger(getClass());
	
	
	//################桥接模式开始############################
	@Override
	public void messageArrived(JConnection conn, T msg) {
		NetConnection gameConn=conn.getAttachment();
		messageArrived(gameConn,msg);
	}

	@Override
	public final void connectionClosed(JConnection connection) {
		NetConnection gameConn=connection.getAttachment();
		connection.setAttachment(null);
		connectionClosed(gameConn);
	}

	@Override
	public final void connectionOpened(JConnection connection) {
		NetConnection gameConn=new NetConntectionAdapter(connection);
		connection.setAttachment(gameConn);
		connectionOpened(gameConn);
	}
	//################桥接模式结束############################

	public abstract void messageArrived(NetConnection conn,T msg);
    
	public abstract void connectionOpened(NetConnection connection);

	public abstract void connectionClosed(NetConnection connection);
}
