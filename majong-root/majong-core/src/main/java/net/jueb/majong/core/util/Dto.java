package net.jueb.majong.core.util;

import net.jueb.majong.core.net.message.ByteBuffer;

public interface Dto {

	public void readFrom(ByteBuffer buffer);
	
	public void writeTo(ByteBuffer buffer);
}
