package net.jueb.majong.script.center.inner.game;

import java.util.Collection;

import net.jueb.majong.center.base.GameController;
import net.jueb.majong.center.manager.GameManager;
import net.jueb.majong.center.manager.RoleGameLockManager;
import net.jueb.majong.core.common.dto.RoleLock.LockServerType;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.script.center.inner.AbstractInnerScript;

@IntMapper(GameMsgCode.Center_GameUnReg)
public class GameUnRegScript extends AbstractInnerScript{

	@Override
	public void action() {
		super.action();
		GameController game=getParam(0);
		if(game!=null)
		{
			int serverId=game.getServerInfo().getServerId();
			GameManager.getInstance().remove(serverId);
			_log.info("游戏服务器移除,game="+game);
			Collection<Long> ids=RoleGameLockManager.getInstance().unlockByServer(serverId);//解锁角色
			broadcastRoleLockUpdate(serverId, LockServerType.Game, false, ids);
			notice_GameServerOffline(serverId);//通知网关断掉服务器连接
		}
	}
	
	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer buffer) {
		
	}
}
