package net.jueb.majong.script.center.inner.game;

import java.util.Map.Entry;

import net.jueb.majong.center.base.GameController;
import net.jueb.majong.center.base.RoleCache;
import net.jueb.majong.center.manager.GameManager;
import net.jueb.majong.center.manager.RoleGameLockManager;
import net.jueb.majong.core.common.dto.Role;
import net.jueb.majong.core.common.dto.RoleChange;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.script.center.inner.AbstractInnerScript;

/**
 * 玩家角色数据更新
 * @author Administrator
 */
@IntMapper(GameMsgCode.Game_RoleDataUpdate)
public class RoleDataUpdateScript extends AbstractInnerScript{

	@Override
	public void action() {
		
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer buffer) {
		long roleId=buffer.readLong();
		RoleChange change=new RoleChange();
		change.readFrom(buffer);
		Role role=RoleCache.getInstance().getRoleById(roleId);
		if(role==null)
		{
			_log.error("更新角色信息失败,role not found,conn="+connection+",change:"+change);
			return;
		}
		GameController gc=GameManager.getInstance().find(connection);
		if(gc!=null)
		{
			int lockedServerId=RoleGameLockManager.getInstance().lockedServer(role.getId());
			if(lockedServerId!=gc.getServerInfo().getServerId())
			{
				_log.error("更新角色信息失败,锁定服务器和当前消息通道服务器不一致,lockedServerId="+lockedServerId+",reqServer="+gc.getServerInfo()+",change:"+change);
				return;
			}
		}
		try {
			_log.debug("角色应用增量:role="+role+"\n"+change);
			applyChange(role, change);
			_log.debug("角色应用增量成功:role="+role+"\n"+change);
			RoleCache.getInstance().updateMark(role.getId());
		} catch (Exception e) {
			_log.debug("角色应用增量失败:role="+role+"\n"+change);
		}
	}
	
	
	/**
	 * 为角色应用增量
	 * @param role
	 * @param change
	 */
	protected void applyChange(Role role,RoleChange change)
	{
		long newMoney=role.getMoney()+change.getMoney();
		if(newMoney<0)
		{
			newMoney=0;
		}
		role.setMoney(newMoney);
		for(Entry<Integer, Integer> e:change.getItems().entrySet())
		{
			role.getConfig()._bagItemAdd(e.getKey(), e.getValue());
		}
	}
}
