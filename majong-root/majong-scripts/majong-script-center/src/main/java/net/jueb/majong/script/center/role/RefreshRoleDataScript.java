package net.jueb.majong.script.center.role;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.jueb.majong.center.base.RoleCache;
import net.jueb.majong.center.manager.RoleGameLockManager;
import net.jueb.majong.core.common.dto.Role;
import net.jueb.majong.core.common.dto.RoleLock.LockServerType;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.net.message.RoleGameMessage;
import net.jueb.majong.core.script.factory.annations.IntMapper;

@IntMapper(GameMsgCode.Center_RoleHallDataRefresh)
public class RefreshRoleDataScript extends AbstractRoleGameActionScript{

	/**
	 * 主动刷新,通知玩家弹回大厅并更新数据
	 */
	public void action() {
		long roleId= getParam(0);
		roleHallDataRefresh(roleId);
	};
	
	RoleGameMessage action;
	@Override
	protected void handleAction(NetConnection connection, RoleGameMessage action) {
		this.action=action;
		roleHallDataRefresh(action.getRoleId());
	}
	
	protected void roleHallDataRefresh(long roleId)
	{
		Role role=RoleCache.getInstance().getRoleById(roleId);
		if(role==null)
		{
			return;
		}
		int lockedServerId=RoleGameLockManager.getInstance().lockedServer(role.getId());//当前玩家所在服务器
		boolean inGameServer= lockedServerId>0;
		//刷新大厅的时候,顺便更新网关的锁
		broadcastRoleLockUpdate(lockedServerId, LockServerType.Game,inGameServer, role.getId());
		ByteBuffer buffer=new ByteBuffer();
		buffer.writeBoolean(inGameServer);//是否在游戏中
		buffer.writeUTF(role.getName());
		buffer.writeLong(role.getMoney());
		buffer.writeUTF(role.getFaceIcon());
		Map<Integer,Integer> item=new HashMap<>();
		for(Entry<Integer,Integer> e:role.getConfig().getBagItem().entrySet())
		{
			item.put(e.getKey(), e.getValue());
		}
		buffer.writeInt(item.size());
		for(Entry<Integer,Integer> e:item.entrySet())
		{
			buffer.writeInt(e.getKey());
			buffer.writeInt(e.getValue());
		}
		responseAction(new GameMessage(GameMsgCode.Center_RoleHallDataRefresh, buffer));
	}
}