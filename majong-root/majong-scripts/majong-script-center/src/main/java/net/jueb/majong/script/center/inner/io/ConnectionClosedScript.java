package net.jueb.majong.script.center.inner.io;

import net.jueb.majong.center.ServerMain;
import net.jueb.majong.center.ServerQueue;
import net.jueb.majong.center.base.GameController;
import net.jueb.majong.center.base.GateController;
import net.jueb.majong.center.factory.ScriptFactory;
import net.jueb.majong.center.manager.GameManager;
import net.jueb.majong.center.manager.GateManager;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.IServerScript;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.script.center.inner.AbstractInnerScript;


@IntMapper(GameMsgCode.Center_ConnectionClosed)
public class ConnectionClosedScript extends AbstractInnerScript{

	@Override
	public void action() {
		NetConnection connection=getParam(0);
		_log.debug("链路关闭:"+connection+",args:"+connection.getAttributeNames());
		GateController gate=GateManager.getInstance().find(connection);
		if(gate!=null)
		{
			IServerScript script=ScriptFactory.getInstance().buildAction(GameMsgCode.Center_GateUnReg, gate);
			ServerMain.getInstance().getQueues().execute(ServerQueue.MAIN,script);
		}
		GameController game=GameManager.getInstance().find(connection);
		if(game!=null)
		{
			IServerScript script=ScriptFactory.getInstance().buildAction(GameMsgCode.Center_GameUnReg, game);
			ServerMain.getInstance().getQueues().execute(ServerQueue.MAIN,script);
		}
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		// TODO Auto-generated method stub
	}
}
