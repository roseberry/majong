package net.jueb.majong.script.center.inner.gate;

import net.jueb.majong.center.base.GateController;
import net.jueb.majong.center.manager.GateManager;
import net.jueb.majong.center.manager.RoleGateLockManager;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.script.center.inner.AbstractInnerScript;

@IntMapper(GameMsgCode.Center_GateUnReg)
public class GateUnRegScript extends AbstractInnerScript{

	@Override
	public void action() {
		super.action();
		GateController gate=(GateController) getParam(0);
		if(gate!=null)
		{
			GateManager.getInstance().remove(gate.getGateInfo().getServerId());
			RoleGateLockManager.getInstance().unlockByServer(gate.getGateInfo().getServerId());//解锁角色
			_log.info("网关移除:"+gate.getGateInfo());
		}
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer msg) {
		// TODO Auto-generated method stub
	}
}
