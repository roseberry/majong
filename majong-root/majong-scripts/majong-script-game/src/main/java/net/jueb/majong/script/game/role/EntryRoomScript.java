package net.jueb.majong.script.game.role;

import net.jueb.majong.core.common.dto.RoomNumber;
import net.jueb.majong.core.common.enums.GameRoomEvent;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameErrCode;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.net.message.RoleGameMessage;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.ServerConfig;
import net.jueb.majong.game.base.RoleController;
import net.jueb.majong.game.base.RoomController;
import net.jueb.majong.game.base.TableLocationEnum;
import net.jueb.majong.game.base.TableStateEnum;
import net.jueb.majong.game.manager.RoleManager;
import net.jueb.majong.game.manager.RoomManager;

/**
 * 
 */
@IntMapper(GameMsgCode.Game_EntryRoom)
public class EntryRoomScript extends AbstractRoleGameActionScript{
	
	@Override
	public void action() {
		
	}
	
	@Override
	protected void handleAction(NetConnection connection, RoleGameMessage action) {
		RoleController role=RoleManager.getInstance().getById(action.getRoleId());
		if(role==null)
		{
			_log.error("role not found by RoleGameMessage:"+action);
			return ;
		}
		ByteBuffer buff=action.getMsg().getContent();
		int roomNumber=buff.readInt();
		RoomNumber num=RoomNumber.valueOf(roomNumber);
		RoomController myroom=role.getRoom();
		if(myroom!=null)
		{//已经存在某房间不能加入
			responseError(GameErrCode.InOtherRoomError);
			return ;
		}
		if(num.getServerId()!=ServerConfig.SERVER_ID)
		{//房间号所属服务器错误
			responseError(GameErrCode.RoomNumberError);
			return ;
		}
		RoomController room=RoomManager.getInstance().getById(num.getRoomId());
		if(room==null)
		{//房间不存在
			responseError(GameErrCode.RoomNotFound);
			return ;
		}
		if(room.getState()!=TableStateEnum.Created)
		{//房间人数已满
			responseError(GameErrCode.RoomRoleLimit);
			return ;
		}
		TableLocationEnum local=null;
		for(TableLocationEnum l:TableLocationEnum.values())
		{
			if(room.getRole(l)==null)
			{//选择一个空位置
				local=l;break;
			}
		}
		if(local==null)
		{
			_log.error("玩家无法找到可用位置,reqnum:"+num+",room="+room);
			responseError(GameErrCode.RoomRoleLimit);
			return ;
		}
		room.setRole(role,local);//设置桌子
		ByteBuffer rsp=new ByteBuffer();
		rsp.writeInt(GameErrCode.Succeed.value());
		rsp.writeUTF(room.getNumber().toNumberString());//房间号
		rsp.writeInt(local.getValue());//自己位置信息
		responseAction(new GameMessage(GameMsgCode.Game_EntryRoom,rsp));
		push_RoomEvent(room.getNumber(), GameRoomEvent.Entry, role.getId());
	}
	
	private void responseError(GameErrCode code)
	{
		ByteBuffer rsp=new ByteBuffer();
		rsp.writeInt(code.value());
		responseAction(new GameMessage(GameMsgCode.Game_EntryRoom,rsp));
		return ;
	}
}