package net.jueb.majong.script.game.innerScript.gate;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.base.GateController;
import net.jueb.majong.game.manager.GateManager;
import net.jueb.majong.script.game.innerScript.AbstractInnerScript;

@IntMapper(GameMsgCode.Game_GateUnReg)
public class GateUnRegScript extends AbstractInnerScript{


	@Override
	public void action() {
		GateController gate=(GateController) getParam(0);
		if(gate!=null)
		{
			GateManager.getInstance().remove(gate.getGateInfo().getServerId());
			_log.info("网关移除:"+gate.getGateInfo());
		}
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer msg) {
		
	}
}
