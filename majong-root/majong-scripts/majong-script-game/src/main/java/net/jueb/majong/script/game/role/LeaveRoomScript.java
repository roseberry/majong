package net.jueb.majong.script.game.role;

import net.jueb.majong.core.common.enums.GameRoomEvent;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameErrCode;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.net.message.RoleGameMessage;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.base.RoleController;
import net.jueb.majong.game.base.RoomController;
import net.jueb.majong.game.base.TableLocationEnum;
import net.jueb.majong.game.base.TableStateEnum;
import net.jueb.majong.game.manager.RoleManager;

/**
 * 
 */
@IntMapper(GameMsgCode.Game_LeaveRoom)
public class LeaveRoomScript extends AbstractRoleGameActionScript{

	@Override
	public void action() {
		
	}
	
	@Override
	protected void handleAction(NetConnection connection, RoleGameMessage action) {
		RoleController role=RoleManager.getInstance().getById(action.getRoleId());
		if(role==null)
		{
			_log.error("role not found by RoleGameMessage:"+action);
			return ;
		}
		RoomController room=role.getRoom();
		if(room==null)
		{//房间不存在
			responseError(GameErrCode.UnSupportOperation);
			return ;
		}
		if(!room.hasRole(role))
		{//不在此房间
			responseError(GameErrCode.UnSupportOperation);
			return ;
		}
		RoleController master=room.getRole(room.getMaster());
		if(master==role)
		{//房主不能离开房间,只能解散房间
			responseError(GameErrCode.UnSupportOperation);
			return ;
		}
		if(room.getState()!=TableStateEnum.Created)
		{//房间此时不可离开
			responseError(GameErrCode.UnSupportOperation);
			return ;
		}
		TableLocationEnum local=room.getLocation(role);
		RoleController old=room.setRole(null, local);
		if(old!=role)
		{//意外情况
			_log.error("玩家离开房间异常,self:"+role+",local="+local+",old="+old+",room="+room);
			room.setRole(old, local);//还原
			responseError(GameErrCode.UnknownError);
			return ;
		}
		ByteBuffer rsp=new ByteBuffer();
		rsp.writeInt(GameErrCode.Succeed.value());
		responseAction(new GameMessage(GameMsgCode.Game_LeaveRoom,rsp));
		push_RoomEvent(room.getNumber(), GameRoomEvent.Exit, role.getId());
	}
	
	private void responseError(GameErrCode code)
	{
		ByteBuffer rsp=new ByteBuffer();
		rsp.writeInt(code.value());
		responseAction(new GameMessage(GameMsgCode.Game_LeaveRoom,rsp));
		return ;
	}
}