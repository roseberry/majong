package net.jueb.majong.script.game.role;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameErrCode;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.net.message.RoleGameMessage;
import net.jueb.majong.core.script.IServerScript;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.game.base.RoleController;
import net.jueb.majong.game.base.RoomController;
import net.jueb.majong.game.factory.ScriptFactory;
import net.jueb.majong.game.manager.RoleManager;

/**
 * 离开游戏服
 */
@IntMapper(GameMsgCode.Game_LeaveGame)
public class LeaveGameScript extends AbstractRoleGameActionScript{

	/**
	 * 系统控制玩家退出
	 */
	@Override
	public void action() {
		RoleController role=(RoleController) getParam(0);
		RoomController room=role.getRoom();
		if(room==null)
		{//房间存在不可离开
			RoleManager.getInstance().remove(role.getId());
			IServerScript script=ScriptFactory.getInstance().buildAction(GameMsgCode.Center_GameRoleExit,role.getId());
			script.run();
		}else
		{
			_log.error("有房间存在,不可离开游戏服,roleId="+role.getId()+",room="+room.getNumber());
		}
	}
	
	/**
	 * 玩家主动退出游戏服务器
	 */
	@Override
	protected void handleAction(NetConnection connection, RoleGameMessage action) {
		RoleController role=RoleManager.getInstance().getById(action.getRoleId());
		if(role==null)
		{
			_log.error("role not found by RoleGameMessage:"+action);
			return ;
		}
		RoomController room=role.getRoom();
		if(room!=null)
		{//房间存在不可离开
			ByteBuffer rsp=new ByteBuffer();
			rsp.writeInt(GameErrCode.UnSupportOperation.value());
			responseAction(new GameMessage(GameMsgCode.Game_LeaveGame,rsp));
			return ;
		}
		RoleManager.getInstance().remove(role.getId());
		role.pushChangeToCenter();
		IServerScript script=ScriptFactory.getInstance().buildAction(GameMsgCode.Center_GameRoleExit,role.getId());
		script.run();
	}
}