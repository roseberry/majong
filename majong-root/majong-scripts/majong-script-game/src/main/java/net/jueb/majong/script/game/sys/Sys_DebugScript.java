package net.jueb.majong.script.game.sys;

import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;

/**
 * 调试脚本
 * @author Administrator
 */
@IntMapper(GameMsgCode.Sys_Debug)
public class Sys_DebugScript extends AbstractSysScript{


	@Override
	public void action() {
		
	}
}
