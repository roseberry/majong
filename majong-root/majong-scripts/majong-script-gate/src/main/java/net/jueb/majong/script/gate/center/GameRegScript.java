package net.jueb.majong.script.gate.center;

import net.jueb.majong.core.common.ServerInfo;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.gate.manager.GameConnectionManager;

@IntMapper(GameMsgCode.Center_GameReg)
public class GameRegScript extends AbstractCenterScript{

	@Override
	public void action() {
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		ServerInfo info=new ServerInfo();
		info.readFrom(clientBuffer);
		_log.info("大厅游戏服务器注册事件:"+info);
		NetConnection conn=GameConnectionManager.getInstance().getServerConnection(info.getServerId());
		if(conn==null || !conn.isActive())
		{
			_log.info("初始化游戏连接:"+info);
			GameConnectionManager.getInstance().initServerConnection(info);
		}
	}
}
