package net.jueb.majong.script.gate.center;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMessage;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.net.message.RoleGameMessage;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.gate.base.RoleAgent;
import net.jueb.majong.gate.manager.RoleAgentManager;
import net.jueb.majong.script.gate.center.AbstractCenterScript;

/**
 * 处理服务器的代理消息并发往客户端
 * @author Administrator
 */
@IntMapper(GameMsgCode.Gate_RoleGameMessage)
public class CenterRoleGameMsgScript extends AbstractCenterScript{

	@Override
	public void action() {
		
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		RoleGameMessage pmsg=new RoleGameMessage();
		pmsg.readFrom(clientBuffer);
		long roleId=pmsg.getRoleId();
		GameMessage msg=pmsg.getMsg();
		RoleAgent ra=RoleAgentManager.getInstance().findAgent(roleId);
		if(ra!=null)
		{
			ra.sendMessage(msg);
			_log.debug("Proxy Center RoleGameMessage="+pmsg);
		}else
		{
			_log.error("Proxy Center RoleGameMessage="+pmsg+",RoleAgent not found"+"]");
		}
	}
}
