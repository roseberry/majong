package net.jueb.majong.script.gate.publicScript.io;

import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.IServerScript;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.gate.ServerMain;
import net.jueb.majong.gate.ServerQueue;
import net.jueb.majong.gate.base.ConnectionKey;
import net.jueb.majong.gate.base.RoleAgent;
import net.jueb.majong.gate.factory.ScriptFactory;
import net.jueb.majong.script.gate.publicScript.AbstractPublicScript;


@IntMapper(GameMsgCode.Gate_ConnectionClosed)
public class ConnectionClosedScript extends AbstractPublicScript{

	@Override
	public void action() {
		NetConnection connection=(NetConnection) getParam(0);
		_log.debug("connectionClosed:"+connection);
		if(connection.hasAttribute(ConnectionKey.RoleAgent))
		{//玩家掉线
			RoleAgent ra=(RoleAgent) connection.getAttribute(ConnectionKey.RoleAgent);
			IServerScript script=ScriptFactory.getInstance().buildAction(GameMsgCode.Gate_RoleDisConnect,ra);
			ServerMain.getInstance().getQueues().execute(ServerQueue.MAIN, script);
		}
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		// TODO Auto-generated method stub
	}
}
