package net.jueb.majong.script.gate.publicScript;

import net.jueb.majong.core.common.enums.RoleGateEvent;
import net.jueb.majong.core.net.NetConnection;
import net.jueb.majong.core.net.message.ByteBuffer;
import net.jueb.majong.core.net.message.GameMsgCode;
import net.jueb.majong.core.script.IServerScript;
import net.jueb.majong.core.script.factory.annations.IntMapper;
import net.jueb.majong.gate.ServerMain;
import net.jueb.majong.gate.ServerQueue;
import net.jueb.majong.gate.base.RoleAgent;
import net.jueb.majong.gate.factory.ScriptFactory;
import net.jueb.majong.gate.manager.RoleAgentManager;
import net.jueb.util4j.cache.map.TimedMap.EventListener;

/**
 * 角色断线
 * @author Administrator
 */
@IntMapper(GameMsgCode.Gate_RoleDisConnect)
public class RoleDisConnectScript extends AbstractPublicScript implements EventListener<Long,RoleAgent>{

	@Override
	public void action() {
		RoleAgent ra=(RoleAgent) getParam(0);
		ra.setLastOffTime(System.currentTimeMillis());
		NetConnection offlineConn=ra.getConnection();
		ra.setConnection(null);
		RoleAgentManager.getInstance().addListener(ra.getRoleId(), this);
		RoleAgentManager.getInstance().updateTTL(ra.getRoleId(), RoleAgent.RECONNECT_TIME);
		RoleAgent.totalDisConnect.incrementAndGet();
		_log.debug("网关角色掉线,等待重连,offlineConn="+offlineConn+"ra="+ra);
		roleGateEvent(ra,RoleGateEvent.Disconnect);
	}

	@Override
	protected void handleRequest(NetConnection connection, ByteBuffer clientBuffer) {
		// TODO Auto-generated method stub
	}

	/**
	 * 当RoleAgent被移除后执行此方法
	 */
	@Override
	public void removed(Long key, RoleAgent value,boolean expire) {
		_log.debug("网关角色掉线,重连超时被移除:"+value);
		IServerScript script=ScriptFactory.getInstance().buildAction(GameMsgCode.Gate_LoginOut, value);
		ServerMain.getInstance().getQueues().execute(ServerQueue.Login, script);
	}
}
